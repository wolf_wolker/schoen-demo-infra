output "kubeconfig" {
  value     = linode_lke_cluster.cluster.kubeconfig
  sensitive = true
}

output "k8s_api_endpoints" {
  value = linode_lke_cluster.cluster.api_endpoints
}

output "k8s_cluster_status" {
  value = linode_lke_cluster.cluster.status
}

output "k8s_cluster_id" {
  value = linode_lke_cluster.cluster.id
}

output "load_balancer_hostname" {
  value = kubernetes_ingress_v1.app_ing.status.0.load_balancer.0.ingress.0.hostname
}

output "load_balancer_ip" {
  value = kubernetes_ingress_v1.app_ing.status.0.load_balancer.0.ingress.0.ip
}

output "app_domain" {
  value = local.app_domain
}
  
output "grafana_domain" {
  value = local.grafana_domain
}