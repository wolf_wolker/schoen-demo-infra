variable "gitlab_agent_token" {
  type = string
}

locals {
  project_id         = "schoen-demo-app"
  region             = "us-central"
  gitlab_agent_name  = "schoen-demo-agent"

  port           = 3000
  replicas       = 3
  version        = "latest"
  image          = "registry.gitlab.com/wolf_wolker/schoen-demo-app"
  app_domain     = "app.schoen-demo.site"
  grafana_domain = "grafana.schoen-demo.site"
}
