SCHOEN DEMO PROJECT INFRA STACK
===============================

REQUIREMENTS
------------

- terraform
- kubectl
- helm
- linode account
- ansible-vault

PROJECT DESCRIPTION
-------------------

This project aims to be a simple demo, not ready for production, of a fully automated cluster creation and provision using terraform.

The project is splitted in two parts:

This repository, using terraform, creates the LKE cluster, installs schoen-demo-app (<https://gitlab.com/wolf_wolker/schoen-demo-app>) using kubernetes provider, and adds some extras using helm charts: prometheus, grafana, gitlab-agent and nginx-ingress.

The schoen-demo-app repository, a dummy typescript app, is configured to, using gitlab pipelines, create new images on each commit and deploy the latest one to the cluster when pushing to the main branch. Some metrics are also exposed through the /metrics endpoint.

SOME CONSIDERATIONS
-------------------

Gitlab agent token is included in this repository, but because of its sensitiveness, it is encrypted using ansible-vault, before running terraform plan/apply you should decrypt it.

```
# this decrypts the vault and creates the .env file
$ ansible-vault decrypt .env.vault --output=.env
# if you have dotenv installed, the .env environmental variables will be loaded automatically, if not, run this
$ source .env
# now you are ready to run terraform plan
```

Ingress uses fake hostnames to access to grafana and the application, you should add them to your local dns sever, your hosts file or as host header when calling the endpoints. The hostnames used are app.schoen-demo.site and grafana.schoen-demo.site

These grafana default credentials should not be used in production environments. `admin / prom-operator`
