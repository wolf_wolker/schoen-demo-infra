provider "linode" {}

provider "kubernetes" {
  host        = linode_lke_cluster.cluster.api_endpoints.0
  config_path = "./.kubeconfig"
}

provider "helm" {
  kubernetes {
    host        = linode_lke_cluster.cluster.api_endpoints.0
    config_path = "./.kubeconfig"
  }
}

# creation of the cluster in linode LKE
resource "linode_lke_cluster" "cluster" {
  label       = local.project_id
  k8s_version = "1.23"
  region      = local.region

  pool {
    type  = "g6-standard-2"
    count = 2

    autoscaler {
      min = 2
      max = 10
    }
  }

  # Prevent the count field from overriding autoscaler-created nodes
  lifecycle {
    ignore_changes = [
      pool.0.count
    ]
  }

  # this will dump the kubeconfig file into a local file [don't commit it]
  provisioner "local-exec" {
    command = "echo ${self.kubeconfig} | base64 -di > ./.kubeconfig"
  }
}

# installation of some charts: gitlab agent, prometheus w/ grafana and nginx ingress. Grafana default password shouldn't be used
resource "helm_release" "gitlab-agent" {
  name             = local.gitlab_agent_name
  repository       = "https://charts.gitlab.io"
  chart            = "gitlab-agent"
  namespace        = "gitlab"
  create_namespace = true
  timeout          = 600
  cleanup_on_fail = true
  atomic = true

  set {
    name  = "image.tag"
    value = "v15.6.0-rc1"
  }
  set {
    name  = "config.kasAddress"
    value = "wss://kas.gitlab.com"
  }
  set {
    name  = "config.token"
    value = var.gitlab_agent_token
  }
}

resource "helm_release" "nginx_ingress" {
  name             = "nginx-ingress-controller"
  repository       = "https://kubernetes.github.io/ingress-nginx"
  chart            = "ingress-nginx"
  namespace        = "nginx-ingress"
  create_namespace = true
  timeout          = 600
  cleanup_on_fail = true
  atomic = true
}

resource "helm_release" "prometheus_stack" {
  name             = "prometheus-stack"
  repository       = "https://prometheus-community.github.io/helm-charts"
  chart            = "kube-prometheus-stack"
  namespace        = "prometheus-stack"
  create_namespace = true
  timeout          = 600
  cleanup_on_fail = true
  atomic = true
  set {
    name  = "grafana.ingress.hosts[0]"
    value = local.grafana_domain
  }
  set {
    name  = "grafana.ingress.path"
    value = "/"
  }
  set {
    name  = "grafana.ingress.ingressClassName"
    value = "nginx"
  }
  set {
    name  = "grafana.ingress.enabled"
    value = true
  }
  depends_on = [helm_release.nginx_ingress, kubernetes_service_v1.app_svc]
}

# install latest version of schoen app
resource "kubernetes_deployment_v1" "app_deployment" {
  metadata {
    name = local.project_id
  }

  spec {
    replicas = local.replicas
    selector {
      match_labels = {
        app = local.project_id
      }
    }
    template {
      metadata {
        annotations = {
          "prometheus.io/path"   = "/metrics"
          "prometheus.io/port"   = local.port
          "prometheus.io/scrape" = true
        }
        labels = {
          app = local.project_id
        }
      }
      spec {
        container {
          image             = "${local.image}:${local.version}"
          image_pull_policy = "Always"
          name              = local.project_id
          port {
            container_port = local.port
          }
          resources {
            limits = {
              cpu    = "200m"
              memory = "320Mi"
            }
            requests = {
              cpu    = "10m"
              memory = "16Mi"
            }
          }
        }
      }
    }
  }

  depends_on = [kubernetes_service_v1.app_svc]
}

resource "kubernetes_service_v1" "app_svc" {
  metadata {
    name = local.project_id
    annotations = {
      "prometheus.io/path"   = "/metrics"
      "prometheus.io/port"   = local.port
      "prometheus.io/scrape" = true
    }
  }
  spec {
    selector = {
      app = local.project_id
    }
    port {
      port        = local.port
      target_port = local.port
      protocol    = "TCP"
    }
    type = "ClusterIP"
  }
}

resource "kubernetes_ingress_v1" "app_ing" {
  wait_for_load_balancer = true
  metadata {
    name = local.project_id
  }
  spec {
    ingress_class_name = "nginx"
    rule {
      host = local.app_domain
      http {
        path {
          path      = "/"
          path_type = "Prefix"
          backend {
            service {
              name = kubernetes_service_v1.app_svc.metadata.0.name
              port {
                number = local.port
              }
            }
          }
        }
      }
    }
  }

  depends_on = [kubernetes_service_v1.app_svc, helm_release.nginx_ingress]
}